<?php $__env->startSection('head.title'); ?>
	Design And Construction
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body.content'); ?>
	<div class="title-box">
		<span class="dot"></span>
		<ul id="breadcrumb">
			<li><a href="<?php echo e(route('service.index')); ?>">Dịch vụ</a></li>
			<li><a href="<?php echo e(route('service.design.index')); ?>">Design And Construction</a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="box" id="service-design">
		<?php foreach($categorys as $contents): ?>
		<div class="gallery-content">
			<h4><?php echo e($contents->name); ?></h4>
			<div class="row">
				<?php foreach($contents->value as $content): ?>
				<div class="col-sm-4">
					<a href="<?php echo e(route('service.design.subcategory',[$contents->url,$content->url_friendly])); ?>/"><img class="img-responsive" src="<?php echo e(URL::asset($content->thumbnail)); ?>" alt="<?php echo e($content->namesub); ?>"></a>
					<p><?php echo e($content->namesub); ?></p>
					<p><a href="<?php echo e(route('service.design.subcategory',[$contents->url,$content->url_friendly])); ?>/">Chi tiết ...</a></p>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>