<div class="container" id="head">
	<div class="row">
		<div class="col-md-4">
			<img class="img-responsive" src="<?php echo e(URL::asset('img/logo.png')); ?>" alt="logo">
		</div>
		<div class="col-md-6 col-md-offset-2">
			<div id="search">
				<form action="#">
					<input type="text" placeholder="Tìm kiếm">
					<button type="submit">Tìm kiếm</button>
				</form>
			</div>
			<div class="clearfix"></div>
			<div id="nav">
				<ul>
					<?php if(array_key_exists(0, $nav)): ?>
						<li><a class="<?php echo e(($nav[0] == '' || $nav[0] == 'home') ? 'nav_active' : ''); ?>" href="<?php echo e(route('home')); ?>">Trang chủ</a></li>
						<li><a class="<?php echo e(($nav[0] == 'about') ? 'nav_active' : ''); ?>" href="<?php echo e(route('about')); ?>">Giới thiệu</a></li>
						<li class="main-menu <?php echo e(($nav[0] == 'service') ? 'nav_active' : ''); ?>">
							<a class="<?php echo e(($nav[0] == 'service') ? 'nav_active' : ''); ?>" href="<?php echo e(route('service.index')); ?>">Dịch vụ</a>
							<ul class="child-menu">
								<?php if(array_key_exists(1, $nav)): ?>
									<li><a id="<?php echo e(($nav[1] == 'trade-fair-exhibition') ? 'child-active' : ''); ?>" href="<?php echo e(route('service.trade.index')); ?>">Trade Fair Exbihition</a></li>
									<?php foreach($list as $valuel): ?>
										<li><a id="<?php echo e(($nav[1] == $valuel['slug']) ? 'child-active' : ''); ?>" href="<?php echo e(route('service.service', ['service' => $valuel['slug']])); ?>"><?php echo e($valuel['name']); ?></a></li>
									<?php endforeach; ?>
								<?php else: ?>
									<li><a href="<?php echo e(route('service.trade.index')); ?>">Trade Fair Exbihition</a></li>
									<?php foreach($list as $valuel): ?>
										<li><a href="<?php echo e(route('service.service', ['service' => $valuel['slug']])); ?>"><?php echo e($valuel['name']); ?></a></li>
									<?php endforeach; ?>
								<?php endif; ?>
							</ul>
						</li>
						<li><a href="#">Liên hệ</a></li>
					<?php else: ?>
						<li><a class="nav_active" href="#">Trang chủ</a></li>
						<li><a href="<?php echo e(route('about')); ?>">Giới thiệu</a></li>
						<li class="main-menu">
							<a href="<?php echo e(route('service.index')); ?>">Dịch vụ</a>
							<ul class="child-menu">
								<li><a href="<?php echo e(route('service.trade.index')); ?>">Trade Fair Exbihition</a></li>
								<?php foreach($list as $valuel): ?>
									<li><a href="<?php echo e(route('service.service', ['service' => $valuel['slug']])); ?>"><?php echo e($valuel['name']); ?></a></li>
								<?php endforeach; ?>
							</ul>
						</li>
						<li><a href="#">Liên hệ</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="clearfix"></div>
			<div id="lang">
				<span><a href="#"><img src="<?php echo e(URL::asset('img/vi.png')); ?>" alt="Tiếng Việt"></a></span>
				<span><a href="#"><img src="<?php echo e(URL::asset('img/en.png')); ?>" alt="Tiếng Anh"></a></span>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>