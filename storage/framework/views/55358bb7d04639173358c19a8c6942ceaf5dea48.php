<div class="col-md-3" id="sidebar">
	<div id="sidebar-category">
		<h4>Dịch vụ</h4>
		<ul>
			<li class="<?php echo e(($link == 1) ? 'active' : ''); ?>"><a href="<?php echo e(route('service.trade.index')); ?>">Trade Fair Exhibition</a></li>
			<?php foreach($resultSidebar as $service): ?>
				<li><a href="<?php echo e(route('service.service', ['service' => $service['slug']])); ?>"><?php echo e($service['name']); ?></a>
					<ul>
						<?php foreach($service['category'] as $category): ?>
							<li><a href="<?php echo e(route('service.category', ['service' => $service['slug'], 'category' => $category['slug']])); ?>"><?php echo e($category['name']); ?></a></li>
						<?php endforeach; ?>
					</ul>
				</li>
			<?php endforeach; ?>
		</ul>
		<script>
				$(document).ready(function() {
					$('#sidebar-category li').each( function() {
						if($(this).children('ul').length > 0){
							$(this).prepend('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>');
							$(this).addClass('parent');
						}else{
							$(this).children('a').css('padding-left','20px');
						}
						if($(this).attr('rel') == 'open'){
							$(this).addClass('active');
						}
					});
					$('#sidebar-category li.parent span').click( function() {
						if($(this).parent().attr('rel') == 'open'){
							$(this).parent().removeClass('active');
							$(this).attr('class', 'glyphicon glyphicon-plus');
							$(this).parent().removeAttr('rel');
						}else{
							$(this).parent().addClass('active');
							$(this).parent().attr('rel', 'open');
							$(this).attr('class', 'glyphicon glyphicon-minus');
						}
					});
				});
			</script>
		<div class="clearfix"></div>
	</div>
	<div id="sidebar-link">
		<h4>trade fair exhibition</h4>
		<ul class="clearfix">
		<?php foreach($showTfe as $show): ?>
			<li><a href="<?php echo e(route('service.trade.show', $show->slug)); ?>"><img src="<?php echo e(URL::asset($show->image)); ?>" alt="<?php echo e($show->name); ?>"></a></li>
		<?php endforeach; ?>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div id="sidebar-link">
		<h4>vietnam news</h4>
		<ul class="clearfix">
			<?php foreach($showPartner as $partner): ?>
			<li><a target="_blank" href="<?php echo e($partner->link); ?>"><img src="<?php echo e(URL::asset($partner->image)); ?>" alt="<?php echo e($partner->description); ?>"></a></li>
			<?php endforeach; ?>
		</ul>
		<div class="clearfix"></div>
	</div>
</div>