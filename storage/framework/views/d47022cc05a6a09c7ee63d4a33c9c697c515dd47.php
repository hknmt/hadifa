<?php $__env->startSection('head.title'); ?>
	<?php echo e($name->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('body.sidebar'); ?>
	<?php echo $__env->make('partials.sidebarService', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body.content'); ?>
	<div class="title-box">
		<span class="dot"></span>
		<ul id="breadcrumb">
			<li><a href="<?php echo e(route('service.index')); ?>">Dịch vụ</a></li>
			<li><a href="<?php echo e(route('service.service', ['service' => $name->slug])); ?>"><?php echo e($name->name); ?></a></li>
			<li><a href="<?php echo e(route('service.category', ['service' => $name->slug, 'category' => $result->slug])); ?>"><?php echo e($result->name); ?></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="box" id="service-design">
		<?php foreach($result->subcategory as $value): ?>
			<div class="gallery-content">
				<div class="gallery-content-title clearfix">
					<p class="pull-right"><a href="<?php echo e(route('service.subcategory',['service' => $name->slug, 'category' => $result->slug, 'subcategory' => $value->slug])); ?>">Xem tất cả</a></p>
					<div class="clearfix"></div>
					<h4><?php echo e($value->name); ?></h4>
				</div>
				<div class="row">
					<?php foreach($value->post as $post): ?>
						<div class="col-md-4">
							<div class="img-thumb">
								<a href="<?php echo e(route('service.post',['service' => $name->slug ,'category' => $result->slug, 'subcategory' => $result->slug, 'post' => $post->slug])); ?>/">
									<img class="img-responsive" src="<?php echo e(URL::asset($post->image)); ?>" alt="<?php echo e($post->name); ?>">
								</a>
							</div>
							<div class="caption">
								<p><?php echo e(str_limit($post->name, 20)); ?></p>
								<p><a href="<?php echo e(route('service.post',['service' => $name->slug ,'category' => $result->slug, 'subcategory' => $value->slug, 'post' => $post->slug])); ?>/">Chi tiết ...</a></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endforeach; ?>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<?php echo $result->subcategory->links(); ?>

			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>