<?php $__env->startSection('head.title'); ?>
Giới Thiệu
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body.sidebar'); ?>
	<?php echo $__env->make('partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body.content'); ?>
<div class="title-box">
	<span class="dot"></span>
	<h3>Giới thiệu</h3>
	<div class="clearfix"></div>
</div>

<div class="box" id="about">
	<img src="<?php echo e(URL::asset($about->image)); ?>" alt="about" class="img-responsive">
	<div id="about-description">
		<p><?php echo e($about->intro); ?></p>
	</div>
	<div class="clearfix"></div>
	<div id="about-content">
		<p><?php echo e($about->content); ?></p>
	</div>					
</div>

<div class="box" id="about-benefit">
	<h4>chúng tôi mang lại cho bạn</h4>
	<div class="row">
		<div class="col-md-4">
			<p><img src="<?php echo e(URL::asset('img/idea.png')); ?>" alt="ý tưởng"></p>
			<p>giải pháp</p>
			<p>tốt nhất</p>
		</div>
		<div class="col-md-4">
			<p><img src="<?php echo e(URL::asset('img/briefcase.png')); ?>" alt="ý tưởng"></p>
			<p>kết quả</p>
			<p>cao nhất</p>
		</div>
		<div class="col-md-4">
			<p><img src="<?php echo e(URL::asset('img/good.png')); ?>" alt="ý tưởng"></p>
			<p>chất lượng</p>
			<p>tốt nhất</p>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>