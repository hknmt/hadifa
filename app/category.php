<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{

	protected $table = 'categories';

	protected $fillable = [
		'name',
		'slug'
	];
    
	/**
	 *Get subcategory for category.
	 */
	public function Subcategorys()
	{
		return $this->hasMany('App\subcategory', 'category_id');
	}

	public function Service()
	{

		return $this->belongsTo('App\Service', 'service_id');

	}

}
