<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Request;
use App\service;
use App\category;
use App\subcategory;
use App\Tfe;

class SidebarServiceComposer
{

	public function __construct()
	{
		//
	}

	public function compose(View $view)
	{
		
		$uri = Request::segments();
		$result = Service::where('slug', $uri[1])->first()->toArray();
		$result['category'] = Service::find($result['id'])->Categories()->get()->toArray();
		foreach($result['category'] as $key => $value) {
			$result['category'][$key]['subcategory'] = Category::find($value['id'])->Subcategorys()->get()->toArray();
		}
		$view->with([
			'resultSidebar' => $result,
			'link'          => $active,
			'showTfe'       => $this->showTFE(),
			'showPartner'   => $this->showPartner()
		]);

	}

	public function showTFE()
	{

		$category = Tfe::all();

		return $category;

	}

	public function showPartner()
	{

		$partner = Partner::all();

		return $partner;

	}

}